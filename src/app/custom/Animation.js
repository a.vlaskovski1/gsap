import EventEmitter from 'eventemitter3';
import selectors from './selectors';
import gsap from 'gsap/all';

export default class Animation extends EventEmitter {
  constructor() {
    super();

    this._tl = new gsap.timeline({ paused: true });
  }

  init() {
    this._addEventListeners();

    this._setListTweens();
    this._setTruckTweens();
  }

  _play() {
    // eslint-disable-next-line no-unused-expressions
    this._tl.isActive() || this._tl.progress() === 1
      ? this._tl.restart()
      : this._tl.play();
  }

  _pause() {
    this._tl.pause();
  }

  _reverse() {
    this._tl.reverse();
  }

  _setListTweens() {
    const { listItems, list } = selectors;

    this._tl.to(list, { duration: 1, y: -100, ease: 'power1.inOut' })
      .to(list, { duration: 0.5, y: 0, ease: 'power1.in' });

    listItems.forEach((item, index) => {
      this._tl.to(item, { duration: 0.3, y: (80 + (index * 40)), ease: 'linear.none', opacity: 0 }, 'items');
    });
  }

  _setTruckTweens() {
    const {
      frontGroup, frontWheel1, frontWheel2, frontWheelsBack,
      backWheel1, backWheel2, containerParts,
      container, backWheelBack1, backWheelBack2, truck, shippedLabel,
      truckBtnBg, truckBtnLabel,
    } = selectors;

    this._tl
      .to([truckBtnBg, truckBtnLabel], { scaleX: 1.1, scaleY: 1.05, transformOrigin: 'center', duration: 0.3 })
      .to([truckBtnBg, truckBtnLabel], { scaleX: 1, scaleY: 1, ease: 'power1.out', duration: 0.3 })
      .to(containerParts, { duration: 0.5, opacity: 1 }, 'container')
      .to(container, { duration: 0.5, opacity: 1 }, 'container')
      .to(backWheel1, { duration: 0.5, opacity: 1 }, 'backWheels')
      .to(backWheel2, { duration: 0.5, opacity: 1 }, 'backWheels')
      .to(backWheelBack1, { duration: 0.5, opacity: 1 }, 'backWheels')
      .to(backWheelBack2, { duration: 0.5, opacity: 1 }, 'backWheels')
      .to(frontGroup, { duration: 1, opacity: 1 }, 'frontGroup')
      .to(frontWheel1, { duration: 1, opacity: 1 }, 'frontGroup')
      .to(frontWheel2, { duration: 1, opacity: 1 }, 'frontGroup')
      .to(frontWheelsBack, { duration: 1, opacity: 1 }, 'frontGroup')
      .to(truck, { duration: 2, opacity: 0, ease: 'back.in(3)', x: 800 })
      .to(shippedLabel, { duration: 0.5, opacity: 1 });
  }

  _addEventListeners() {
    const { truckBtn, playBtn, pauseBtn, reverseBtn } = selectors;

    truckBtn.addEventListener('click', () => this._play());
    playBtn.addEventListener('click', () => this._play());
    pauseBtn.addEventListener('click', () => this._pause());
    reverseBtn.addEventListener('click', () => this._reverse());
  }
}
